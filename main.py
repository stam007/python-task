from fastapi import FastAPI
from pydantic import BaseModel
from fastapi.responses import JSONResponse
app = FastAPI()


class Get_Path(BaseModel):
    From_Node:int
    To_Node:int




graph = {5: [11],
             11: [10, 9, 2],
             7: [11,8],
             8: [9],
             3: [10,8],
             10: [],
             2:[]}


@app.get("/")
async def root():
    return {"graph": graph}



def find_path(graph, start, end, path=[]):
        path = path + [start]
        if start == end:
            return path
        if start not in graph:
            return None
        for node in graph[start]:
            if node not in path:
                newpath = find_path(graph, node, end, path)
                if newpath: return newpath
        return None

@app.post("/get_path/")
async def get_path(data:Get_Path):

    return find_path(graph, data.From_Node,data.To_Node)  
    
